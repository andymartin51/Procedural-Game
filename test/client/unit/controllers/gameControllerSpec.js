describe('when gameController is loaded',function() {
    // load angular.module and the desired controller and its dependencies
    var ctrl, $scope, $httpBackend, $routeParams, $timeout;
    beforeEach(module('rmpApp'));
    beforeEach(inject(function ($controller, $injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        $httpBackend = $injector.get('$httpBackend');
        $routeParams = $injector.get('$routeParams');
        $timeout = $injector.get('$timeout');
        ctrl = $controller('gameController', { $scope: $scope });
    }));
    // load mocks and other variables
    var hello;
    beforeEach(function() {
        hello = window.jsonMocks['_example'];
    });

    describe('initialize game controller', function() {
        it('should call a function to initialize some scope variables', function() {
            spyOn($scope,'init');
            $scope.init();
            expect($scope.init).toHaveBeenCalled();
        });
        it('should initialize the tiles array', function() {
            $scope.init();
            expect(Array.isArray($scope.tiles)).toBe(true);
        });
        it('should initialize the action queue array', function() {
            $scope.init();
            expect(Array.isArray($scope.actionQueue)).toBe(true);
        });
        it('should start a new game', function() {
            spyOn($scope,'startGame');
            $scope.init();
            expect($scope.startGame).toHaveBeenCalled();
        });
    });

    describe('start a new game', function() {
        beforeEach(function() {
            $scope.init();
        });
        it('should call a function to start the game', function() {
            spyOn($scope,'startGame');
            $scope.startGame();
            expect($scope.startGame).toHaveBeenCalled();
        });
        it('should build the tile map', function() {
            spyOn($scope,'buildTileMap');
            $scope.startGame();
            expect($scope.buildTileMap).toHaveBeenCalled();
        });
        it('should add the player', function() {
            spyOn($scope,'createPlayer');
            $scope.createPlayer();
            expect($scope.createPlayer).toHaveBeenCalled();
        });
    });

    describe('build array of game tiles to be displayed and navigated', function() {
        it('should call a function to build a map of game tiles', function() {
            spyOn($scope,'buildTileMap');
            $scope.buildTileMap();
            expect($scope.buildTileMap).toHaveBeenCalled();
        });
        it('should create an array of tiles', function() {
            $scope.buildTileMap();
            expect(Array.isArray($scope.tiles)).toBe(true);
        });
        it('should initialize the size of the tile map', function() {
            $scope.buildTileMap();
            expect(Object.keys($scope.tileMap)).toEqual(['sizeX','sizeY']);
        });
        it('should create a 20x15 grid of tiles (640x480 screen @ 32x32 tiles)', function() {
            $scope.buildTileMap();
            expect($scope.tiles.length).toBe($scope.tileMap.sizeX * $scope.tileMap.sizeY);
        });
        it('should make tile objects with specified properties', function() {
            $scope.buildTileMap();
            expect(Object.keys($scope.tiles[0])).toEqual(["x","y","unit"]);
        });
    });

    describe('create the player and add to the map', function() {
        beforeEach(function() {
            $scope.init();
        });
        it('should call a function to create the player object', function() {
            spyOn($scope,'createPlayer');
            $scope.createPlayer();
            expect($scope.createPlayer).toHaveBeenCalled();
        });
        it('should initialize a player object', function() {
            $scope.createPlayer();
            expect(typeof $scope.player).toBe('object');
        });
        it('should add the player object to the tile map', function() {
            spyOn($scope,'addUnitToTileByCoord');
            $scope.createPlayer();
            expect($scope.addUnitToTileByCoord).toHaveBeenCalledWith($scope.player);
        });
        it('should set the index value in the player object to the tile array index of (x,y) coord', function() {
            $scope.createPlayer();
            expect($scope.player.index).toBeGreaterThan(0);
        });
    });

    describe('add a unit to the map', function() {
        beforeEach(function() {
            $scope.init();
        });
        it('should call a function to add the unit into the map array', function() {
            spyOn($scope,'addUnitToTileByCoord');
            $scope.addUnitToTileByCoord();
            expect($scope.addUnitToTileByCoord).toHaveBeenCalled();
        });
        it('should punt if the "unit" param is not a valid object', function() {
            // valid in this case is defined as having both an x and y value
            expect($scope.addUnitToTileByCoord()).toBe(false);
            expect($scope.addUnitToTileByCoord('hello')).toBe(false);
            expect($scope.addUnitToTileByCoord({x:5})).toBe(false);
        });
        it('should put the unit into the tiles array at the (x,y) coordinate', function() {
            var bsObj = {x:2,y:3};
            $scope.addUnitToTileByCoord(bsObj);
            var filter = $scope.tiles.filter(function(elem) {
                return elem.x === bsObj.x && elem.y === bsObj.y;
            });
            expect(filter[0].unit.x).toBe(bsObj.x);
            expect(filter[0].unit.y).toBe(bsObj.y);
        });
    });

    describe('remove a unit from the map', function() {
        beforeEach(function() {
            $scope.init();
            $scope.buildTileMap();
            u = {x: 2, y: 3};
            $scope.addUnitToTileByCoord(u);
        });
        it('should call a function to remove a unit from a tile', function() {
            spyOn($scope,'removeUnitFromTileByCoord');
            $scope.removeUnitFromTileByCoord();
            expect($scope.removeUnitFromTileByCoord).toHaveBeenCalled();
        });
        it('should punt if unit is invalid', function() {
            expect($scope.removeUnitFromTileByCoord()).toBe(false);
            expect($scope.removeUnitFromTileByCoord('hello')).toBe(false);
            expect($scope.removeUnitFromTileByCoord({x:1})).toBe(false);
        });
        it('should remove the unit property from the tile in the array', function() {
            $scope.removeUnitFromTileByCoord(u);
            var filter = $scope.tiles.filter(function(elem) {
                return elem.x === u.x && elem.y === u.y;
            });
            expect(filter[0].unit).toBeUndefined();
        });
    });

    describe('add selected action to a queue (array)', function() {
        beforeEach(function() {
            $scope.init();
        });
        it('should call a function to add the action to the queue', function() {
            spyOn($scope,'addActionToQueue');
            $scope.addActionToQueue();
            expect($scope.addActionToQueue).toHaveBeenCalled();
        });
        it('should punt if the action param is invalid', function() {
            expect($scope.addActionToQueue()).toBe(false);
            expect($scope.addActionToQueue(1)).toBe(false);
            expect($scope.addActionToQueue({x:2})).toBe(false);
        });
        it('should punt if the action param is not in the list of accepted actions', function() {
            expect($scope.addActionToQueue('go left')).toBe(false);
        });
        it('should push the action param into an array', function() {
            $scope.addActionToQueue('Turn Left');
            expect($scope.actionQueue[0]).toBe('Turn Left');
        });
        it('(UI) should set a flag to disable the pick up action button', function() {
            $scope.addActionToQueue('Pick Up');
            expect($scope.pickUpQueued).toBe(true);
        });
        it('(UI) should set a flag to disable the drop off action button', function() {
            $scope.addActionToQueue('Drop Off');
            expect($scope.dropOffQueued).toBe(true);
        });
        it('should execute the action queue if the Drop Off action is added', function() {
            spyOn($scope,'executeQueue');
            $scope.addActionToQueue('Drop Off');
            expect($scope.executeQueue).toHaveBeenCalled();
        });
    });

    describe('reset the action queue', function() {
        beforeEach(function() {
            $scope.init();
        });
        it('should call a function to reset the queue', function() {
            spyOn($scope,'resetQueue');
            $scope.resetQueue();
            expect($scope.resetQueue).toHaveBeenCalled();
        });
        it('should recreate the held relic on the map if it was not dropped successfully', function() {
            spyOn($scope,'addUnitToTileByCoord');
            spyOn($scope,'createPlayer');
            $scope.player.heldRelic = {x: 1, y: 2};
            $scope.resetQueue();
            expect($scope.addUnitToTileByCoord).toHaveBeenCalledWith($scope.player.heldRelic);
        });
        it('should reset the flag that expresses that the drop off was successful', function() {
            $scope.resetQueue();
            expect($scope.dropSuccess).toBe(false);
        });
        it('should reinitialize the action queue array', function() {
            $scope.addActionToQueue('Turn Left');
            $scope.resetQueue();
            expect($scope.actionQueue.length).toBe(0);
        });
        it('should return the player to the starting position', function() {
            spyOn($scope,'removeUnitFromTileByCoord');
            spyOn($scope,'createPlayer');
            $scope.resetQueue();
            expect($scope.removeUnitFromTileByCoord).toHaveBeenCalledWith($scope.player);
            expect($scope.createPlayer).toHaveBeenCalled();
        });
    });

    describe('execute the actions in the queue', function() {
        beforeEach(function() {
            $scope.init();
            $scope.executeTimer = $timeout(function() {});
        });
        it('should call a function to execute the queue', function() {
            spyOn($scope,'executeQueue');
            $scope.executeQueue();
            expect($scope.executeQueue).toHaveBeenCalled();
        });
        it('(UI) should set a flag to disable the action buttons while executing', function() {
            $scope.actionQueue = [];
            $scope.executeQueue();
            expect($scope.executing).toBe(true);
        });
        it('should consume the actions in the array as First In First Out', function() {
            $scope.addActionToQueue('Turn Left');
            $scope.addActionToQueue('Turn Right');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.actionsPerformed[0]).toBe('Turn Left');
        });
        it('should make a call to change facing (left) if the action is Turn Left', function() {
            spyOn($scope,'changeFacing');
            $scope.addActionToQueue('Turn Left');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.changeFacing).toHaveBeenCalledWith($scope.player,'L');
        });
        it('should make a call to change facing (right) if the action is Turn Right', function() {
            spyOn($scope,'changeFacing');
            $scope.addActionToQueue('Turn Right');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.changeFacing).toHaveBeenCalledWith($scope.player,'R');
        });
        it('should make a call to move unit (forward) if the action is Move Forward', function() {
            spyOn($scope,'moveUnit');
            $scope.addActionToQueue('Move Forward');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.moveUnit).toHaveBeenCalledWith($scope.player,'F');
        });
        it('should make a call to move unit (backward) if the action is Move Backward', function() {
            spyOn($scope,'moveUnit');
            $scope.addActionToQueue('Move Backward');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.moveUnit).toHaveBeenCalledWith($scope.player,'B');
        });
        it('should make a call to pick up relic if the action is Pick Up', function() {
            spyOn($scope,'pickUpRelic');
            $scope.addActionToQueue('Pick Up');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.pickUpRelic).toHaveBeenCalled();
        });
        it('should make a call to drop off relic if the action is Drop Off', function() {
            spyOn($scope,'dropOffRelic');
            $scope.addActionToQueue('Drop Off');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.dropOffRelic).toHaveBeenCalled();
        });
        it('should make a call to the postqueue handler when the action queue is emptied', function() {
            spyOn($scope,'postQueueHandler');
            $scope.addActionToQueue('Turn Left');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.success).toBe(true);
            expect($scope.postQueueHandler).toHaveBeenCalled();
        });
        it('should make a call to the postqueue handler when the timeout is canceled', function() {
            spyOn($scope,'postQueueHandler');
            $scope.player.x = 0;
            $scope.player.facing = 'W';
            $scope.addActionToQueue('Move Forward');
            $scope.executeQueue();
            $timeout.flush();
            expect($scope.success).toBe(false);
            expect($scope.postQueueHandler).toHaveBeenCalled();
        });
    });

    describe('act after the queue is finished executing', function() {
        beforeEach(function() {
            $scope.init();
            $scope.addActionToQueue('Turn Left');
        });
        it('should call a function to handle what happens after the queue is finished', function() {
            spyOn($scope,'postQueueHandler');
            $scope.postQueueHandler();
            expect($scope.postQueueHandler).toHaveBeenCalled();
        });
        it('should reset the queue', function() {
            spyOn($scope,'resetQueue');
            $scope.postQueueHandler();
            expect($scope.resetQueue).toHaveBeenCalled();
        });
        it('(UI) should set a flag to enable the action buttons once again', function() {
            $scope.postQueueHandler();
            expect($scope.executing).toBe(false);
        });
        it('(UI) should set a flag to enable the pick up action button once again', function() {
            $scope.postQueueHandler();
            expect($scope.pickUpQueued).toBe(false);
        });
        it('(UI) should set a flag to enable the pick up action button once again', function() {
            $scope.postQueueHandler();
            expect($scope.dropOffQueued).toBe(false);
        });
    });

    describe('change the direction that the unit is facing', function() {
        beforeEach(function() {
            $scope.init();
        });
        it('should call a function to turn the unit', function() {
            spyOn($scope,'changeFacing');
            $scope.changeFacing();
            expect($scope.changeFacing).toHaveBeenCalled();
        });
        it('should update the unit facing based on turn direction and current facing', function() {
            $scope.changeFacing($scope.player, 'R');
            expect($scope.player.facing).toBe('E');
        });
        it('should punt if the direction provided is invalid', function() {
            expect($scope.changeFacing()).toBe(false);
            expect($scope.changeFacing($scope.player)).toBe(false);
            expect($scope.changeFacing($scope.player,1)).toBe(false);
            expect($scope.changeFacing($scope.player,'right')).toBe(false);
        });
        it('should make the unit face N if the unit is facing E and turns L', function() {
            $scope.player.facing = 'E';
            $scope.changeFacing($scope.player, 'L');
            expect($scope.player.facing).toBe('N');
        });
        it('should make the unit face W if the unit is facing N and turns L', function() {
            $scope.player.facing = 'N';
            $scope.changeFacing($scope.player, 'L');
            expect($scope.player.facing).toBe('W');
        });
        it('should make the unit face S if the unit is facing W and turns L', function() {
            $scope.player.facing = 'W';
            $scope.changeFacing($scope.player, 'L');
            expect($scope.player.facing).toBe('S');
        });
        it('should make the unit face E if the unit is facing S and turns L', function() {
            $scope.player.facing = 'S';
            $scope.changeFacing($scope.player, 'L');
            expect($scope.player.facing).toBe('E');
        });
        it('should make the unit face S if the unit is facing E and turns R', function() {
            $scope.player.facing = 'E';
            $scope.changeFacing($scope.player, 'R');
            expect($scope.player.facing).toBe('S');
        });
        it('should make the unit face E if the unit is facing N and turns R', function() {
            $scope.player.facing = 'N';
            $scope.changeFacing($scope.player, 'R');
            expect($scope.player.facing).toBe('E');
        });
        it('should make the unit face N if the unit is facing W and turns R', function() {
            $scope.player.facing = 'W';
            $scope.changeFacing($scope.player, 'R');
            expect($scope.player.facing).toBe('N');
        });
        it('should make the unit face W if the unit is facing S and turns R', function() {
            $scope.player.facing = 'S';
            $scope.changeFacing($scope.player, 'R');
            expect($scope.player.facing).toBe('W');
        });
    });

    describe('move a unit forward or backward', function() {
        beforeEach(function() {
            $scope.init();
            $scope.player.facing = 'N';
            $scope.player.y = 1;
        });
        it('should call a function to move a unit through the map', function() {
            spyOn($scope,'moveUnit');
            $scope.moveUnit();
            expect($scope.moveUnit).toHaveBeenCalled();
        });
        it('should check if a move is possible in the direction specified', function() {
            spyOn($scope,'checkMoveDirection');
            $scope.moveUnit($scope.player,'F');
            expect($scope.checkMoveDirection).toHaveBeenCalled();
        });
        it('should cancel the queue execution if a move is not possible in the direction specified', function() {
            spyOn($timeout,'cancel');
            $scope.player.y = 0;
            $scope.moveUnit($scope.player,'F');
            expect($timeout.cancel).toHaveBeenCalled();
        });
        it('should cancel the move if a move is not possible in the direction specified', function() {
            $scope.player.y = 0;
            expect($scope.moveUnit($scope.player,'F')).toBe(false);
        });
        it('should not reset the queue if the unit specified is not the player', function() {
            spyOn($scope,'resetQueue');
            var u = {x:0, y:1, facing: 'W'};
            $scope.moveUnit(u, 'F');
            expect($scope.resetQueue).not.toHaveBeenCalled();
            expect($scope.moveUnit(u, 'F')).toBe(false);
        });
        it('should remove the unit from the current tile before changing coordinate', function() {
            spyOn($scope,'removeUnitFromTileByCoord');
            $scope.moveUnit($scope.player,'F');
            expect($scope.removeUnitFromTileByCoord).toHaveBeenCalled();
        });
        it('should adjust the Y value by -1 if facing N and direction F', function() {
            $scope.player.y = 1;
            $scope.player.facing = 'N';
            $scope.moveUnit($scope.player,'F');
            expect($scope.player.y).toBe(0);
        });
        it('should adjust the Y value by +1 if facing S and direction F', function() {
            $scope.player.y = 1;
            $scope.player.facing = 'S';
            $scope.moveUnit($scope.player,'F');
            expect($scope.player.y).toBe(2);
        });
        it('should adjust the X value by -1 if facing W and direction F', function() {
            $scope.player.x = 1;
            $scope.player.facing = 'W';
            $scope.moveUnit($scope.player,'F');
            expect($scope.player.x).toBe(0);
        });
        it('should adjust the X value by +1 if facing E and direction F', function() {
            $scope.player.x = 1;
            $scope.player.facing = 'E';
            $scope.moveUnit($scope.player,'F');
            expect($scope.player.x).toBe(2);
        });
        it('should adjust the Y value by +1 if facing N and direction B', function() {
            $scope.player.y = 1;
            $scope.player.facing = 'N';
            $scope.moveUnit($scope.player,'B');
            expect($scope.player.y).toBe(2);
        });
        it('should adjust the Y value by -1 if facing S and direction B', function() {
            $scope.player.y = 1;
            $scope.player.facing = 'S';
            $scope.moveUnit($scope.player,'B');
            expect($scope.player.y).toBe(0);
        });
        it('should adjust the X value by +1 if facing W and direction B', function() {
            $scope.player.x = 1;
            $scope.player.facing = 'W';
            $scope.moveUnit($scope.player,'B');
            expect($scope.player.x).toBe(2);
        });
        it('should adjust the X value by -1 if facing E and direction B', function() {
            $scope.player.x = 1;
            $scope.player.facing = 'E';
            $scope.moveUnit($scope.player,'B');
            expect($scope.player.x).toBe(0);
        });
        it('should add the unit to the tile where it ends up', function() {
            spyOn($scope,'addUnitToTileByCoord');
            $scope.moveUnit($scope.player,'F');
            expect($scope.addUnitToTileByCoord).toHaveBeenCalled();
        });
    });

    describe('check the intended space of a move for any potential collision', function() {
        var enemy = {x: 3, y: 3};
        beforeEach(function() {
            $scope.init();
            $scope.addUnitToTileByCoord(enemy);
        });
        it('should call a function to check the intended direction for any existing unit', function() {
            spyOn($scope,'checkMoveDirection');
            $scope.checkMoveDirection();
            expect($scope.checkMoveDirection).toHaveBeenCalled();
        });
        it('should punt if direction is invalid', function() {
            expect($scope.checkMoveDirection()).toBe(false);
            expect($scope.checkMoveDirection($scope.player,'J')).toBe(false);
            expect($scope.checkMoveDirection($scope.player,{x:2})).toBe(false);
        });
        it('should punt if direction is F and unit is facing W at X = 0', function() {
            $scope.player.facing = 'W';
            $scope.player.x = 0;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is F and unit is facing E at X = tileMap.sizeX-1', function() {
            $scope.player.facing = 'E';
            $scope.player.x = $scope.tileMap.sizeX-1;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is F and unit is facing N at Y = 0', function() {
            $scope.player.facing = 'N';
            $scope.player.y = 0;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is F and unit is facing S at Y = tileMap.sizeY-1', function() {
            $scope.player.facing = 'S';
            $scope.player.y = $scope.tileMap.sizeY-1;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is B and unit is facing E at X = 0', function() {
            $scope.player.facing = 'E';
            $scope.player.x = 0;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is B and unit is facing W at X = tileMap.sizeX-1', function() {
            $scope.player.facing = 'W';
            $scope.player.x = $scope.tileMap.sizeX-1;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is B and unit is facing S at Y = 0', function() {
            $scope.player.facing = 'S';
            $scope.player.y = 0;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is B and unit is facing N at Y = tileMap.sizeY-1', function() {
            $scope.player.facing = 'N';
            $scope.player.y = $scope.tileMap.sizeY-1;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is F and unit is facing E with another unit having unit.x+1', function() {
            $scope.player.facing = 'E';
            $scope.player.x = 2;
            $scope.player.y = 3;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is F and unit is facing W with another unit having unit.x-1', function() {
            $scope.player.facing = 'W';
            $scope.player.x = 4;
            $scope.player.y = 3;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is F and unit is facing S with another unit having unit.y+1', function() {
            $scope.player.facing = 'S';
            $scope.player.x = 3;
            $scope.player.y = 2;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is F and unit is facing N with another unit having unit.y-1', function() {
            $scope.player.facing = 'N';
            $scope.player.x = 3;
            $scope.player.y = 4;
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(false);
        });
        it('should punt if direction is B and unit is facing E with another unit having unit.x-1', function() {
            $scope.player.facing = 'E';
            $scope.player.x = 4;
            $scope.player.y = 3;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is B and unit is facing W with another unit having unit.x+1', function() {
            $scope.player.facing = 'W';
            $scope.player.x = 2;
            $scope.player.y = 3;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is B and unit is facing S with another unit having unit.y-1', function() {
            $scope.player.facing = 'S';
            $scope.player.x = 3;
            $scope.player.y = 4;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should punt if direction is B and unit is facing N with another unit having unit.y+1', function() {
            $scope.player.facing = 'N';
            $scope.player.x = 3;
            $scope.player.y = 2;
            expect($scope.checkMoveDirection($scope.player,'B')).toBe(false);
        });
        it('should succeed if the unit would move into an unobstructed space', function() {
            $scope.player.x = 0;
            $scope.player.y = 0;
            $scope.player.facing = 'S';
            expect($scope.checkMoveDirection($scope.player,'F')).toBe(true);
        });
    });

    describe('attempt to pick up a relic in front of the player', function() {
        var relic;
        beforeEach(function() {
            $scope.init();
            relic = {
                x: 18,
                y: 13,
                type: 'relic'
            };
            $scope.addUnitToTileByCoord(relic);
        });
        it('should call a function to attempt to pick up a relic', function() {
            spyOn($scope,'pickUpRelic');
            $scope.pickUpRelic();
            expect($scope.pickUpRelic).toHaveBeenCalled();
        });
        it('should punt if the player already has a held relic', function() {
            spyOn($scope,'resetQueue');
            $scope.player.heldRelic = {x:2};
            expect($scope.pickUpRelic()).toBe(false);
            expect($scope.resetQueue).toHaveBeenCalled();
        });
        it('should check for a relic 1 tile to the N if player is facing N', function() {
            $scope.player.facing = 'N';
            $scope.pickUpRelic();
            expect($scope.player.heldRelic).toBeDefined();
        });
        it('should check for a relic 1 tile to the S if player is facing S', function() {
            $scope.player.facing = 'S';
            $scope.player.y = 12;
            $scope.pickUpRelic();
            expect($scope.player.heldRelic).toBeDefined();
        });
        it('should check for a relic 1 tile to the E if player is facing E', function() {
            $scope.player.facing = 'E';
            $scope.player.x = 17;
            $scope.player.y = 13;
            $scope.pickUpRelic();
            expect($scope.player.heldRelic).toBeDefined();
        });
        it('should check for a relic 1 tile to the W if player is facing W', function() {
            $scope.player.facing = 'W';
            $scope.player.x = 19;
            $scope.player.y = 13;
            $scope.pickUpRelic();
            expect($scope.player.heldRelic).toBeDefined();
        });
        it('should punt if the player is attempting to pick up out of bounds', function() {
            $scope.player.facing = 'S';
            $scope.pickUpRelic();
            expect($scope.relicExists).toBe(false);
        });
        it('should punt if no relic exists in front of the player', function() {
            $scope.player.facing = 'E';
            $scope.pickUpRelic();
            expect($scope.relicExists).toBe(false);
        });
        it('should punt if a unit exists in front of the player that is not a relic', function() {
            $scope.player.facing = 'W';
            var u = { x: 17, y: 14 };
            $scope.addUnitToTileByCoord(u);
            $scope.pickUpRelic();
            expect($scope.relicExists).toBe(false);
        });
        it('should give the player the relic to hold if it exists in front', function() {
            $scope.pickUpRelic();
            expect($scope.player.heldRelic).toEqual(relic);
        });
        it('should remove the relic from the map while it is held', function() {
            $scope.pickUpRelic();
            expect($scope.tiles[$scope.relicIndex].unit).toBeUndefined();
        });
        it('should cancel the queue execution if no relic exists (failed action)', function() {
            spyOn($timeout, 'cancel');
            $scope.removeUnitFromTileByCoord(relic);
            $scope.pickUpRelic();
            expect($timeout.cancel).toHaveBeenCalled();
        });
    });

    describe('attempt to drop off a held relic', function() {
        var dropoff, relic;
        beforeEach(function() {
            $scope.init();
            dropoff = { x: 18, y: 13, type: 'dropoff', relics: [] };
            relic = { x: 18, y: 8, type: 'relic' };
            $scope.addUnitToTileByCoord(dropoff);
            $scope.player.heldRelic = relic;
        });
        it('should call a function to attempt the drop off', function() {
            spyOn($scope,'dropOffRelic');
            $scope.dropOffRelic();
            expect($scope.dropOffRelic).toHaveBeenCalled();
        });
        it('should punt if the player has no held relic', function() {
            spyOn($scope,'resetQueue');
            $scope.player.heldRelic = undefined;
            expect($scope.dropOffRelic()).toBe(false);
            expect($scope.resetQueue).toHaveBeenCalled();
        });
        it('should check for a dropoff 1 tile to the N if player is facing N', function() {
            $scope.player.facing = 'N';
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(true);
        });
        it('should check for a dropoff 1 tile to the S if player is facing S', function() {
            $scope.player.facing = 'S';
            $scope.player.y = 12;
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(true);
        });
        it('should check for a dropoff 1 tile to the E if player is facing E', function() {
            $scope.player.facing = 'E';
            $scope.player.x = 17;
            $scope.player.y = 13;
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(true);
        });
        it('should check for a dropoff 1 tile to the W if player is facing W', function() {
            $scope.player.facing = 'W';
            $scope.player.x = 19;
            $scope.player.y = 13;
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(true);
        });
        it('should punt if the player is attempting to drop off out of bounds', function() {
            $scope.player.facing = 'S';
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(false);
        });
        it('should punt if no dropoff exists in front of the player', function() {
            $scope.player.facing = 'W';
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(false);
        });
        it('should punt if a unit exists in front of the player that is not a dropoff', function() {
            $scope.player.facing = 'W';
            var u = { x: 17, y: 14 };
            $scope.addUnitToTileByCoord(u);
            $scope.dropOffRelic();
            expect($scope.dropoffExists).toBe(false);
        });
        it('should add the held relic to the relics array in the dropoff', function() {
            $scope.dropOffRelic();
            expect($scope.dropOff.relics[0]).toEqual(relic);
        });
        it('should remove the held relic from the player object', function() {
            $scope.dropOffRelic();
            expect($scope.player.heldRelic).toBeUndefined();
        });
        it('should set a flag to express that the drop off was successful', function() {
            $scope.dropOffRelic();
            expect($scope.dropSuccess).toBe(true);
        });
        it('should celebrate the successful drop off', function() {
            spyOn($scope,'celebrate');
            $scope.dropOffRelic();
            expect($scope.celebrate).toHaveBeenCalled();
        });
        it('should cancel the queue execution if no dropoff exists (failed action)', function() {
            spyOn($timeout,'cancel');
            $scope.player.facing = 'S';
            $scope.dropOffRelic();
            expect($timeout.cancel).toHaveBeenCalled();
        });
    });

    describe('celebrate a successful dropoff', function() {
        var dropoff, relic;
        beforeEach(function() {
            $scope.init();
            for(var i in $scope.tiles) {
                if($scope.tiles[i].unit && $scope.tiles[i].unit.type === 'relic') {
                    $scope.tiles[i].unit = undefined;
                }
            }
            dropoff = { x: 18, y: 13, type: 'dropoff', relics: [] };
            relic = { x: 18, y: 8, type: 'relic' };
            $scope.addUnitToTileByCoord(dropoff);
            $scope.addUnitToTileByCoord(relic);
        });
        it('should call a function to celebrate the successful drop off', function() {
            spyOn($scope,'celebrate');
            $scope.celebrate();
            expect($scope.celebrate).toHaveBeenCalled();
        });
        it('should tell the player how many relics are left if more than zero', function() {
            spyOn(window,'alert');
            $scope.celebrate();
            expect(window.alert).toHaveBeenCalledWith('Successful drop! 1 relic remains.');
        });
        it('should tell the player that they won if there are no relics left', function() {
            spyOn(window,'alert');
            $scope.removeUnitFromTileByCoord(relic);
            $scope.celebrate();
            expect(window.alert).toHaveBeenCalledWith('You won the whole map!');
        });
    });
});