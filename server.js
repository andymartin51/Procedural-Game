var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mongoose = require('mongoose');
mongoose.connect('mongodb://admin:admin@ds039411.mongolab.com:39411/generated_game');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function(callback) {
	console.log('it works!');
});

var fs = require('fs');

// TODO: this is where the base for CSS/JS files is... add variable to be set for deploy ('/public/')
app.use(express.static(process.cwd() + '/client_src/'));
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true}));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', process.cwd());
// needs to be after app.use statements or bodyParser will not work
var routes = require('./api/routes/routes')(app, fs);

var server = app.listen(process.env.PORT || 3000, function() {
    console.log('Listening on port %d', server.address().port);
});
