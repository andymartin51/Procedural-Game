rmpApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', { controller:'gameController', templateUrl: 'modules/game/views/game.html' });
    $routeProvider.when('/login', { controller: 'gameController', templateUrl: 'modules/game/views/login.html'});
    $routeProvider.otherwise({redirectTo:'/'});
}]);
