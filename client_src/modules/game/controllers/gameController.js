rmpApp.controller('gameController', ['$scope', '$timeout', function($scope, $timeout) {
    // TODO: make "Drop Off" execute the command script instead of "Execute" button

    $scope.init = function() {
        $scope.player = {};
        $scope.tiles = [];
        $scope.actionQueue = [];

        $scope.startGame();
    };

    $scope.startGame = function() {
        $scope.buildTileMap();
        $scope.resetQueue();
        // junk debug wall units added to test failing moves, etc
        for(var i = 12; i > 2; i--) {
            $scope.addUnitToTileByCoord({
                x: i,
                y: i,
                graphic: 'glyphicon-th-large',
                type: 'wall'
            });
        }
    };

    $scope.buildTileMap = function() {
        // TODO: load map from json (pref. by GET/POST from api)
        // types of tiles: (0 player) (1 wall), (2 relic), (3 drop point), (4? enemy)
        $scope.tiles = [];
        $scope.tileMap = {
            sizeX: 20,
            sizeY: 15
        }
        for(var y = 0; y < $scope.tileMap.sizeY; y++) {
            for(var x = 0; x < $scope.tileMap.sizeX; x++) {
                var newTile = {
                    x: x,
                    y: y,
                    unit: undefined
                }
                $scope.tiles.push(newTile);
            }
        }
        $scope.addUnitToTileByCoord({ x: 18, y: 9, graphic: 'glyphicon-gift', type: 'relic' });
        $scope.addUnitToTileByCoord({ x: 5, y: 7, graphic: 'glyphicon-gift', type: 'relic' });
        $scope.addUnitToTileByCoord({ x: 16, y: 2, graphic: 'glyphicon-record', type: 'dropoff', relics: [] });
    };

    $scope.createPlayer = function() {
        $scope.player = {
            x: 18,
            y: 14,
            facing: 'N',
            graphic: 'glyphicon-chevron-up',
            heldRelic: undefined
        };
        $scope.addUnitToTileByCoord($scope.player);
    };

    $scope.addUnitToTileByCoord = function(unit) {
        if(!unit || typeof unit !== 'object') return false;
        if(unit.x === undefined || unit.y === undefined) return false;
        $scope.tiles = $scope.tiles.filter(function(elem,ind) {
            if(elem.x === unit.x && elem.y === unit.y) {
                unit.index = ind;
                elem.unit = unit;
            }
            return true; // keep all members of array
        });
    };

    $scope.removeUnitFromTileByCoord = function(unit) {
        if(!unit || typeof unit !== 'object') return false;
        if(unit.x === undefined || unit.y === undefined) return false;
        $scope.tiles = $scope.tiles.filter(function(elem) {
            if(elem.x === unit.x && elem.y === unit.y) {
                elem.unit = undefined;
            }
            return true;
        });
    };

    $scope.addActionToQueue = function(action) {
        var acceptedActions = [
            'Turn Left',
            'Turn Right',
            'Move Forward',
            'Move Backward',
            'Pick Up',
            'Drop Off'
        ];
        if(acceptedActions.indexOf(action) > -1) $scope.actionQueue.push(action);
        if(action === 'Pick Up') $scope.pickUpQueued = true;
        if(action === 'Drop Off') {
            $scope.dropOffQueued = true;
            $scope.executeQueue();
        }
        // console.log($scope.actionQueue);
        return false;
    };

    $scope.resetQueue = function() {
        if($scope.player.heldRelic !== undefined && !$scope.dropSuccess) $scope.addUnitToTileByCoord($scope.player.heldRelic);
        $scope.dropSuccess = false;
        $scope.actionQueue = [];
        $scope.removeUnitFromTileByCoord($scope.player);
        $scope.createPlayer();
    };

    $scope.executeQueue = function() {
        // TODO: if action fails, move the player backward to return to origin
        $scope.executing = true;
        $scope.actionsPerformed = [];
        // TODO: still feels buggy- the more actions in the queue
        // after it cancels, the more buggy this gets
        // Need a solution that goes through each action without
        // having knowledge of the length of the entire array
        for(var i in $scope.actionQueue) {
            $scope.executeTimer = $timeout(function() {
                var shifted = $scope.actionQueue.shift();
                $scope.actionsPerformed.push(shifted);
                switch(shifted) {
                    case 'Turn Left':
                        $scope.changeFacing($scope.player,'L');
                        break;
                    case 'Turn Right':
                        $scope.changeFacing($scope.player,'R');
                        break;
                    case 'Move Forward':
                        $scope.moveUnit($scope.player,'F');
                        break;
                    case 'Move Backward':
                        $scope.moveUnit($scope.player,'B');
                        break;
                    case 'Pick Up':
                        $scope.pickUpRelic();
                        break;
                    case 'Drop Off':
                        $scope.dropOffRelic();
                        break;
                }
            }, 500 * i);
        }
        $scope.executeTimer.then(function() {
            // console.log('success');
            $scope.success = true;
            $scope.postQueueHandler();
        }, function() {
            // console.log('canceled');
            $scope.success = false;
            $scope.postQueueHandler();
        });
    };

    $scope.postQueueHandler = function() {
        $scope.executing = false;
        $scope.pickUpQueued = false;
        $scope.dropOffQueued = false;
        $scope.resetQueue();
    };

    $scope.changeFacing = function(unit, direction) {
        // direction should be "L" or "R" (left, right)
        if(direction === 'L') {
            switch(unit.facing) {
                case 'N':
                    unit.facing = 'W';
                    unit.graphic = 'glyphicon-chevron-left'
                    break;
                case 'E':
                    unit.facing = 'N';
                    unit.graphic = 'glyphicon-chevron-up'
                    break;
                case 'S':
                    unit.facing = 'E';
                    unit.graphic = 'glyphicon-chevron-right'
                    break;
                case 'W':
                    unit.facing = 'S';
                    unit.graphic = 'glyphicon-chevron-down'
                    break;
            }
        }
        if(direction === 'R') {
            switch(unit.facing) {
                case 'N':
                    unit.facing = 'E';
                    unit.graphic = 'glyphicon-chevron-right'
                    break;
                case 'E':
                    unit.facing = 'S';
                    unit.graphic = 'glyphicon-chevron-down'
                    break;
                case 'S':
                    unit.facing = 'W';
                    unit.graphic = 'glyphicon-chevron-left'
                    break;
                case 'W':
                    unit.facing = 'N';
                    unit.graphic = 'glyphicon-chevron-up'
                    break;
            }
        }
        return false;
        // console.log(unit);
    };

    $scope.moveUnit = function(unit, direction) {
        if(!$scope.checkMoveDirection(unit, direction)) {
            // in the future, other units will be able to move as well
            if(unit == $scope.player) {
                $timeout.cancel($scope.executeTimer);
            }
            return false;
        }
        // remove the unit from the current tile before moving and adding to new tile
        $scope.removeUnitFromTileByCoord(unit);
        switch(unit.facing) {
            case 'N':
                unit.y += direction === 'F' ? -1 : 1;
                break;
            case 'E':
                unit.x += direction === 'F' ? 1 : -1;
                break;
            case 'S':
                unit.y += direction === 'F' ? 1 : -1;
                break;
            case 'W':
                unit.x += direction === 'F' ? -1 : 1;
                break;
        }
        $scope.addUnitToTileByCoord(unit);
        // console.log(unit);
    };

    $scope.checkMoveDirection = function(unit, direction) {
        // prevent unit from leaving screen boundaries
        switch(direction) {
            case 'F':
                if(unit.x === 0 && unit.facing === 'W') return false;
                if(unit.x === $scope.tileMap.sizeX-1 && unit.facing === 'E') return false;
                if(unit.y === 0 && unit.facing === 'N') return false;
                if(unit.y === $scope.tileMap.sizeY-1 && unit.facing === 'S') return false;
                break;
            case 'B':
                if(unit.x === 0 && unit.facing === 'E') return false;
                if(unit.x === $scope.tileMap.sizeX-1 && unit.facing === 'W') return false;
                if(unit.y === 0 && unit.facing === 'S') return false;
                if(unit.y === $scope.tileMap.sizeY-1 && unit.facing === 'N') return false;
                break;
            default:
                return false;
        }
        // prevent unit from moving into another unit (return true if no collision)
        var unitCollision = $scope.tiles.some(function(elem) {
            switch(unit.facing) {
                case 'N':
                    if(elem.x === unit.x && elem.y === unit.y + (direction === 'F' ? -1 : 1)) return !elem.unit;
                    break;
                case 'E':
                    if(elem.x === unit.x + (direction === 'F' ? 1 : -1) && elem.y === unit.y) return !elem.unit;
                    break;
                case 'S':
                    if(elem.x === unit.x && elem.y === unit.y + (direction === 'F' ? 1 : -1)) return !elem.unit;
                    break;
                case 'W':
                    if(elem.x === unit.x + (direction === 'F' ? -1 : 1) && elem.y === unit.y) return !elem.unit;
            }
        });
        if(!unitCollision) return false;
        return true;
    };

    $scope.pickUpRelic = function() {
        if($scope.player.heldRelic) {
            $scope.resetQueue();
            return false;
        }
        var diffX = 0, diffY = 0;
        if($scope.player.facing === 'N') diffY = -1;
        if($scope.player.facing === 'S') diffY = 1;
        if($scope.player.facing === 'E') diffX = 1;
        if($scope.player.facing === 'W') diffX = -1;
        $scope.relicIndex = undefined;
        $scope.relicFound = undefined;
        $scope.relicExists = $scope.tiles.some(function(elem, ind) {
            if(elem.x === $scope.player.x + diffX && elem.y === $scope.player.y + diffY) {
                if(elem.unit) {
                    if(elem.unit.type === 'relic') {
                        $scope.relicIndex = ind;
                        $scope.relicFound = elem.unit;
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        });
        if($scope.relicExists) {
            $scope.player.heldRelic = $scope.relicFound;
            $scope.tiles[$scope.relicIndex].unit = undefined;
        } else {
            $timeout.cancel($scope.executeTimer);
        }
    };

    $scope.dropOffRelic = function() {
        if(!$scope.player.heldRelic) {
            $scope.resetQueue();
            return false;
        }
        var diffX = 0, diffY = 0;
        if($scope.player.facing === 'N') diffY = -1;
        if($scope.player.facing === 'S') diffY = 1;
        if($scope.player.facing === 'E') diffX = 1;
        if($scope.player.facing === 'W') diffX = -1;
        $scope.dropOff = undefined;
        $scope.dropoffExists = $scope.tiles.some(function(elem, ind) {
            if(elem.x === $scope.player.x + diffX && elem.y === $scope.player.y + diffY) {
                if(elem.unit && elem.unit.type === 'dropoff') {
                    $scope.dropOff = elem.unit;
                    return true;
                }
                return false;
            }
        });
        if($scope.dropoffExists) {
            $scope.dropOff.relics.push($scope.player.heldRelic);
            $scope.player.heldRelic = undefined;
            $scope.dropSuccess = true;
            $scope.celebrate();
            // console.log(dropOff.relics);
        } else {
            $timeout.cancel($scope.executeTimer);
        }
    };

    $scope.celebrate = function() {
        $scope.relicsLeft = $scope.tiles.filter(function(elem) {
            return elem.unit && elem.unit.type === 'relic';
        });
        if($scope.relicsLeft.length > 0) {
            alert('Successful drop! '+$scope.relicsLeft.length+' relic'+($scope.relicsLeft.length>1?'s remain':' remains')+'.');
        } else {
            alert('You won the whole map!');
        }
    };
}]);
